---
layout: home

hero:
    name:    nano/modules/services
    tagline: microservices handler for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/services is a microservices handler for nano
    -   title:   services
        details: |
                 nano/modules/services provides a simple way to use microservices in your nano application
---

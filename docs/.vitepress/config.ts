import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/services/',
    },
    title:         'nano/modules/services',
    titleTemplate: 'nano/modules/services - documentation',
    description:   'microservices for nano',
    base:          '/modules/services/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'defineServicesDefaultTransport',
                        link: '/usage/define-services-default-transport',
                    },
                    {
                        text: 'defineService',
                        link: '/usage/define-service',
                    },
                    {
                        text: 'useService',
                        link: '/usage/use-service',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/services',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/services/-/edit/dev/docs/:path',
        },
    },
});

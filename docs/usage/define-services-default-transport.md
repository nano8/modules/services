---
layout: doc
---

<script setup>
const args = {
    defineServicesDefaultTransport: [
        { name: 'transport', type: 'ClientInterface' },
    ],
};
</script>

##### laylatichy\nano\modules\services

## usage

`defineServicesDefaultTransport` is a function that defines the default transport for services

## <Types fn="defineServicesDefaultTransport" r="void" :args="args.defineServicesDefaultTransport" /> {#defineServicesDefaultTransport}

```php
// .config/services.php
$transport = new GuzzleHttp\Client();

defineServicesDefaultTransport($transport);
```


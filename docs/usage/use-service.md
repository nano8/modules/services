---
layout: doc
---

<script setup>
const args = {
    useService: [
        { name: 'class', type: 'string' },
    ],
    withClient: [
        { name: 'client', type: 'ClientInterface' },
    ],
    get: [
        { name: 'path', type: 'string' },
        { name: 'query', type: 'array' },
    ],
    post: [
        { name: 'path', type: 'string' },
        { name: 'body', type: 'array' },
    ],
    put: [
        { name: 'path', type: 'string' },
        { name: 'body', type: 'array' },
    ],
    delete: [
        { name: 'path', type: 'string' },
        { name: 'body', type: 'array' },
    ],
    send: [
        { name: 'method', type: 'Types' },
        { name: 'path', type: 'string' },
        { name: 'body', type: 'array' },
    ],
};
</script>

##### laylatichy\nano\modules\services

## usage

`useService` is a function that returns a service instance

## <Types fn="useService" r="Service" :args="args.useService" /> {#useService}

```php
useService(ExampleService::class);
```

## <Types fn="withClient" r="Service" :args="args.withClient" /> {#withClient}

set the client for the service, by default it will use module default transport

```php
useService(ExampleService::class)
    ->withClient(new GuzzleHttp\Client());
```

## <Types fn="get" r="Response" :args="args.get" /> {#get}

```php
useRouter()->get(
    '/example', 
    fn (Request $request): Response => useService(ExampleService::class)
        ->get('/example')
);
```

## <Types fn="post" r="Response" :args="args.post" /> {#post}

```php
useRouter()->post(
    '/example', 
    fn (Request $request): Response => useService(ExampleService::class)
        ->post('/example', $request->post())
);
```

## <Types fn="put" r="Response" :args="args.put" /> {#put}

```php
useRouter()->put(
    '/example', 
    fn (Request $request): Response => useService(ExampleService::class)
        ->put('/example', $request->post())
);
```

## <Types fn="delete" r="Response" :args="args.delete" /> {#delete}

```php
useRouter()->delete(
    '/example', 
    fn (Request $request): Response => useService(ExampleService::class)
        ->delete('/example', $request->post())
);
```

## <Types fn="send" r="Response" :args="args.send" /> {#send}

```php
useRouter()->get(
    '/example', 
    fn (Request $request): Response => useService(ExampleService::class)
        ->send(Types::GET, '/example')
);
```


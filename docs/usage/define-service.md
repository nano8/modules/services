---
layout: doc
---

<script setup>
const args = {
    defineService: [
        { name: 'service', type: 'Service' },
    ],
};
</script>

##### laylatichy\nano\modules\services

## usage

`defineService` is a function that defines a service

## <Types fn="defineService" r="void" :args="args.defineService" /> {#defineService}

```php
// .config/services.php
class ExampleService extends Service {
    public string $name = 'jsonapi';

    public string $url  = 'https://jsonplaceholder.typicode.com';
}

defineService(new ExampleService());
```


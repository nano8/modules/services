---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-services
```

## registering module

```php
use laylatichy\nano\modules\services\ServicesModule;

useNano()->withModule(new ServicesModule());

// define your handlers here or in a .config/services.php file
```

<?php

use GuzzleHttp\ClientInterface;
use laylatichy\nano\modules\services\Service;
use laylatichy\nano\modules\services\Services;
use laylatichy\nano\modules\services\ServicesModule;

if (!function_exists('useServices')) {
    function useServices(): Services {
        return useNanoModule(ServicesModule::class)
            ->services;
    }
}

if (!function_exists('defineServicesDefaultTransport')) {
    function defineServicesDefaultTransport(ClientInterface $transport): void {
        useServices()->withTransport($transport);
    }
}

if (!function_exists('defineService')) {
    function defineService(Service $service): void {
        useServices()->withService($service);
    }
}

if (!function_exists('useService')) {
    /**
     * @template T of Service
     *
     * @param class-string<T> $class
     */
    function useService(string $class): Service {
        return useServices()->getService($class);
    }
}


<?php

namespace laylatichy\nano\modules\services;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\response\Response;
use laylatichy\nano\core\router\Types;

abstract class Service {
    public string $name;

    public string $url;

    public ClientInterface $client;

    public function __construct() {
        $this->withClient(useServices()->transport);
    }

    public function withClient(ClientInterface $client): self {
        $this->client = $client;

        return $this;
    }

    public function get(string $path, array $query = []): Response {
        return $this->send(Types::GET, $path, [
            'query' => $query,
        ]);
    }

    public function post(string $path, array $body = []): Response {
        return $this->send(Types::POST, $path, [
            'json' => $body,
        ]);
    }

    public function put(string $path, array $body = []): Response {
        return $this->send(Types::PUT, $path, [
            'json' => $body,
        ]);
    }

    public function delete(string $path, array $body = []): Response {
        return $this->send(Types::DELETE, $path, [
            'json' => $body,
        ]);
    }

    public function send(Types $method, string $path, array $params = []): Response {
        try {
            $response = $this->client->request($method->name, $this->url . $path, $params);

            return useResponse()
                ->withCode(HttpCode::from($response->getStatusCode()))
                ->withJson(
                    // @phpstan-ignore-next-line
                    json_decode(
                        $response->getBody()->getContents(),
                        true
                    )
                );
        } catch (BadResponseException $e) {
            return useResponse()
                ->withCode(HttpCode::from($e->getCode()))
                ->withJson(
                    // @phpstan-ignore-next-line
                    json_decode(
                        $e->getResponse()->getBody()->getContents(),
                        true
                    )
                );
        } catch (ConnectException $e) {
            return useResponse()
                ->withCode(HttpCode::INTERNAL_SERVER_ERROR)
                ->withJson([
                    'response' => "service {$this->name} is not available",
                ]);
        }
    }
}

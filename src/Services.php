<?php

namespace laylatichy\nano\modules\services;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class Services {
    /**
     * @var array<class-string<Service>, Service>
     */
    public array $services = [];

    public ClientInterface $transport;

    public function __construct() {
        $this->withTransport(new Client());
    }

    public function withTransport(ClientInterface $transport): self {
        $this->transport = $transport;

        return $this;
    }

    public function withService(Service $service): self {
        $this->services[$service::class] = $service;

        return $this;
    }

    /**
     * @template T of Service
     *
     * @param class-string<T> $class
     */
    public function getService(string $class): Service {
        return $this->services[$class];
    }
}

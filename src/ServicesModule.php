<?php

namespace laylatichy\nano\modules\services;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class ServicesModule implements NanoModule {
    public Services $services;

    public function __construct() {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->services)) {
            useNanoException('services module already registered');
        }

        $this->services = new Services();

        $this->load();
    }

    private function load(): void {
        $file = useConfig()->getRoot() . '/../.config/services.php';

        if (file_exists($file)) {
            require $file;
        }
    }
}
